<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<!-- Mirrored from kodeforest.net/html/books/library/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 18 Jan 2019 08:49:30 GMT -->
<head>
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title><?php bloginfo('name'); ?></title>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="description" content="<?php bloginfo('description'); ?>">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- CUSTOM STYLE -->
<link href="<?php echo get_template_directory_uri(); ?>/css/style.css" rel="stylesheet">
<!-- THEME TYPO -->
<link href="<?php echo get_template_directory_uri(); ?>/css/themetypo.css" rel="stylesheet">
<!-- BOOTSTRAP -->
<link href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.css" rel="stylesheet">
<!-- COLOR FILE -->
<link href="<?php echo get_template_directory_uri(); ?>/css/color.css" rel="stylesheet">
<!-- FONT AWESOME -->
<link href="<?php echo get_template_directory_uri(); ?>/css/font-awesome.min.css" rel="stylesheet">
<!-- BX SLIDER -->
<link href="<?php echo get_template_directory_uri(); ?>/css/jquery.bxslider.css" rel="stylesheet">
<!-- Boostrap Slider -->
<link href="<?php echo get_template_directory_uri(); ?>/css/bootstrap-slider.css" rel="stylesheet">
<!-- Widgets -->
<link href="<?php echo get_template_directory_uri(); ?>/css/widget.css" rel="stylesheet">
<!-- Owl Carusel -->
<link href="<?php echo get_template_directory_uri(); ?>/css/owl.carousel.css" rel="stylesheet">
<!-- responsive -->
<link href="<?php echo get_template_directory_uri(); ?>/css/responsive.css" rel="stylesheet">
<!-- Component -->
<link href="<?php echo get_template_directory_uri(); ?>/css/component.css" rel="stylesheet">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div id="loader-wrapper">
	<div id="loader"></div>

	<div class="loader-section section-left"></div>
	<div class="loader-section section-right"></div>

</div>
<!--WRAPPER START-->
<div class="wrapper kode-header-class-3">
	<!--HEADER START-->
	<header class="header-3">
    	<div class="container">
            <div class="logo-container">
            	<div class="row">
                	<div class="col-md-3">
                    	 <!--LOGO START-->
                        <div class="logo">
                            <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/logo-2.png" alt=""></a>
                        </div>
                        <!--LOGO END-->
                    </div>
                    <div class="col-md-9">
                    	<div class="top-strip">
                            <div class="pull-left">
                                <p>Welcome to Library theme</p>
                            </div>
                            <div class="social-icon">
                                <a href="mailto:info@library-theme.com" class="pull-left">info@library-theme.com</a>
                                <ul>
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>                      
                                </ul>
                            </div>
                        </div>
                    	<div class="kode-navigation">
						   <ul>
								<li><a href="index.html">Home</a>
									<ul>
										<li><a href="index-1.html">Home page 1</a></li>
									</ul>
								</li>
								
								<li><a href="about-us.html">About Us</a></li>
								<li><a href="books.html">Our Books</a>
									<ul>
										<li><a href="books3-sidebar.html">Book With Sidebar</a></li>
										<li><a href="books-detail.html">Book Detail</a></li>                                
									</ul>
								</li>
								<li><a href="blog.html">Blog</a>
									<ul>
										<li><a href="blog-2column.html">Blog 2 Column</a></li>
										<li><a href="blog-full.html">Blog Full</a></li>
										<li><a href="blog-detail.html">Blog Detail</a></li>
									</ul>
								</li>
								<li><a href="authors.html">Authors</a>
									<ul>
										<li><a href="authors.html">Authors</a></li>
										<li><a href="author-detail.html">Authors Detail</a></li>										
									</ul>
								</li>
								<li class="last"><a href="#">Events</a>
									<ul>
										<li><a href="events-2column.html">Event 2 Column</a></li>
										<li><a href="events-3column.html">Event 3 Column</a></li>
										<li><a href="event-full.html">Event Single</a></li>
										<li><a href="event-detail.html">Event Detail</a></li>
									</ul>
								</li>
								
								<li class="last"><a href="#">Pages</a>
									<ul class="children">
										<li><a href="error-404.html">Error 404</a></li>
										<li><a href="coming-soon.html">Comming Soon</a></li>
										<li class="last"><a href="gallery-2col.html">Gallery</a>
											<ul>
												<li><a href="gallery-2col.html">Gallery 2 Col</a></li>
												<li><a href="gallery-3col.html">Gallery 3 Col</a></li>
												<li><a href="gallery-4col.html">Gallery 4 Col</a></li>    
											</ul>
										</li>
									</ul>
								</li>
								<li class="last"><a href="contact-us.html">Contact Us</a></li>
							</ul>
						</div>
						<div id="kode-responsive-navigation" class="dl-menuwrapper">
							<button class="dl-trigger">Open Menu</button>
							<ul class="dl-menu">
								<li class="menu-item kode-parent-menu"><a href="index.html">Home</a>
									<ul class="dl-submenu">
										<li><a href="index-1.html">Home page 1</a></li>
									</ul>
								</li>
								<li><a href="about-us.html">About Us</a></li>
								<li class="menu-item kode-parent-menu"><a href="books.html">Our Books</a>
									<ul class="dl-submenu">
										<li><a href="books3-sidebar.html">Book With Sidebar</a></li>
										<li><a href="books-detail.html">Book Detail</a></li>                                
									</ul>
								</li>
								<li class="menu-item kode-parent-menu"><a href="blog.html">Blog</a>
									<ul class="dl-submenu">
										<li><a href="blog-2column.html">Blog 2 Column</a></li>
										<li><a href="blog-full.html">Blog Full</a></li>
										<li><a href="blog-detail.html">Blog Detail</a></li>
									</ul>
								</li>
								<li class="menu-item kode-parent-menu last"><a href="authors.html">Authors</a>
									<ul class="dl-submenu">
										<li><a href="authors.html">Authors</a></li>
										<li><a href="author-detail.html">Authors Detail</a></li>										
									</ul>
								</li>
								<li class="menu-item kode-parent-menu last"><a href="#">Events</a>
									<ul class="dl-submenu">
										<li><a href="events-2column.html">Event 2 Column</a></li>
										<li><a href="events-3column.html">Event 3 Column</a></li>
										<li><a href="event-full.html">Event Single</a></li>
										<li><a href="event-detail.html">Event Detail</a></li>
									</ul>
								</li>
								<li class="menu-item kode-parent-menu last"><a href="#">Pages</a>
									<ul class="dl-submenu">
										<li><a href="error-404.html">Error 404</a></li>
										<li><a href="coming-soon.html">Comming Soon</a></li>
										<li class="menu-item kode-parent-menu last"><a href="gallery-2col.html">Gallery</a>
											<ul class="dl-submenu">
												<li><a href="gallery-2col.html">Gallery 2 Col</a></li>
												<li><a href="gallery-3col.html">Gallery 3 Col</a></li>
												<li><a href="gallery-4col.html">Gallery 4 Col</a></li>    
											</ul>
										</li>
									</ul>
								</li>
								<li class="last"><a href="contact-us.html">Contact Us</a></li>
							</ul>
						</div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!--HEADER END-->
<!-- <!DOCTYPE html>
<html >
<head>
<meta charset="" />
<meta name="viewport" content="width=device-width" />
<link rel="stylesheet" type="text/css"  />

</head>
<body >
<div id="wrapper" class="hfeed">
<header id="header" role="banner">
<section id="branding">
<div id="site-title"><?php if ( is_front_page() || is_home() || is_front_page() && is_home() ) { echo '<h1>'; } ?><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_html( get_bloginfo( 'name' ) ); ?>" rel="home"><?php echo esc_html( get_bloginfo( 'name' ) ); ?></a><?php if ( is_front_page() || is_home() || is_front_page() && is_home() ) { echo '</h1>'; } ?></div>
<div id="site-description"><?php bloginfo( 'description' ); ?></div>
</section>
<nav id="menu" role="navigation">
<div id="search">
<?php get_search_form(); ?>
</div>
<?php wp_nav_menu( array( 'theme_location' => 'main-menu' ) ); ?>
</nav>
</header>
<div id="container"> -->