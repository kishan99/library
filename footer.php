<footer class="footer-2">
    	<div class="container">
        	<div class="lib-copyrights">
                <p><?php echo sprintf( __( '%1$s %2$s %3$s. All Rights Reserved.', 'blankslate' ), '&copy;', date( 'Y' ), esc_html( get_bloginfo( 'name' ) ) ); echo sprintf( __( ' Theme By: %1$s.', 'blankslate' ), '<a href="http://tidythemes.com/">TidyThemes</a>' ); ?></p>
                <!-- Copyrights © 2015-16 KodeForest. All rights reserved -->
                <div class="social-icon">
                	<ul>
                    	<li><a href="#" data-toggle="tooltip" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#" data-toggle="tooltip" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
                        <li><a href="#" data-toggle="tooltip" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#" data-toggle="tooltip" title="Tumblr"><i class="fa fa-tumblr"></i></a></li>
                    </ul>
                </div>
            </div>
			<div class="back-to-top">
				<a href="#home"><i class="fa fa-angle-up"></i></a>
			</div>
        </div>
    </footer>
</div>
<!--WRAPPER END-->
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="<?php echo get_template_directory_uri(); ?>/js/modernizr.custom.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.bxslider.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap-slider.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/waypoints.min.js"></script> 
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.counterup.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/owl.carousel.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/dl-menu/jquery.dlmenu.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/lib/hash.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/lib/booklet-lib.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquerypp.custom.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.bookblock.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/functions.js"></script>
<?php wp_footer(); ?>
</body>

<!-- Mirrored from kodeforest.net/html/books/library/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 18 Jan 2019 08:49:30 GMT -->
</html>

<!-- <div class="clear"></div>
</div>
<footer id="footer" role="contentinfo">
<div id="copyright">

</div>
</footer>
</div>

</body>
</html> -->